# Telegram bot platform

This is an abstraction layer for developing Telegram bots. It's based on behavior model, similar to behavior tree pattern.

## Installation

1. Add a repository to your pom.xml:

```xml

<repository>
    <id>tsmiatacz-bananarama</id>
    <url>https://packagecloud.io/tsmiatacz/bananarama/maven2</url>
    <releases>
        <enabled>true</enabled>
    </releases>
    <snapshots>
        <enabled>false</enabled>
    </snapshots>
</repository>
```

2. Add `botplatform` dependency:

```xml

<dependency>
    <artifactId>botplatform</artifactId>
    <groupId>pl.tsmiatacz</groupId>
    <version>2.0.0</version>
</dependency>
```

3. Refer to [botexample](https://bitbucket.org/tsmiatacz/botexample/) repository for further instructions.

## Licensing

This code is licensed under the [MIT License](https://opensource.org/licenses/MIT)  