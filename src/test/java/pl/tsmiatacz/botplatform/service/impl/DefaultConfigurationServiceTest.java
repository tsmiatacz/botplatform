package pl.tsmiatacz.botplatform.service.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import pl.tsmiatacz.botplatform.service.ConfigurationService;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class DefaultConfigurationServiceTest {

    private ConfigurationService testObj = new DefaultConfigurationService("test");

    @Test
    void shouldReturnListOfEnabledBots() {
        // given

        // when
        final List<String> result = testObj.getEnabledBots();

        // then
        assertThat(result).containsExactly("Bot1", "Bot2");
    }

    @Test
    void shouldReturnGroupId() {
        // given

        // when
        final String result = testObj.getGroupId("test");

        // then
        assertThat(result).isEqualTo("-80085");
    }

    @Test
    void shouldReturnBotAdmin() {
        // given

        // when
        final String result = testObj.getBotAdmin("TestBot");

        // then
        assertThat(result).isEqualTo("test_bot_admin");
    }

    @Test
    void shouldReturnBotName() {
        // given

        // when
        final String result = testObj.getBotName("TestBot");

        // then
        assertThat(result).isEqualTo("test_bot");
    }

    @Test
    void shouldReturnBotKey() {
        // given

        // when
        final String result = testObj.getBotKey("TestBot");

        // then
        assertThat(result).isEqualTo("test:bot:key");
    }
}