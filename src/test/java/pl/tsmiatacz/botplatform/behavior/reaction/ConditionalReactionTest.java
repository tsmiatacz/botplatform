package pl.tsmiatacz.botplatform.behavior.reaction;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import java.util.function.Predicate;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class ConditionalReactionTest {

    private ConditionalReaction testObj;

    @Mock
    private Predicate<BehaviorContext> condition;
    @Mock(name = "reaction")
    private Reaction reaction;
    @Mock(name = "denial")
    private Reaction denial;

    @Mock
    private BehaviorContext context;

    @BeforeEach
    void setUp() {
        initMocks(this);
        testObj = ConditionalReaction.of(condition, reaction, denial);
    }


    @Test
    void shouldReactWhenConditionIsTrue() {
        // given
        when(condition.test(context)).thenReturn(true);

        // when
        testObj.accept(context);

        // then
        verify(reaction).accept(context);
        verify(denial, never()).accept(context);
    }

    @Test
    void shouldDenyWhenConditionIsFalse() {
        // given
        when(condition.test(context)).thenReturn(false);

        // when
        testObj.accept(context);

        // then
        verify(reaction, never()).accept(context);
        verify(denial).accept(context);
    }
}