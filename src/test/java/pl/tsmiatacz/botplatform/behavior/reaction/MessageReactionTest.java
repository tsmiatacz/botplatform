package pl.tsmiatacz.botplatform.behavior.reaction;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class MessageReactionTest {

    @Mock
    private BehaviorContext context;

    @Captor
    private ArgumentCaptor<SendMessage> methodCaptor;

    @BeforeEach
    void setUp() {
        when(context.getChatId()).thenReturn("1234");
    }

    @Test
    void shouldReactWithMessage() {
        // given
        final MessageReaction testObj = MessageReaction.simple("some message");

        // when
        testObj.accept(context);

        // then
        verify(context).execute(methodCaptor.capture());
        final SendMessage message = methodCaptor.getValue();
        assertThat(message.getChatId()).isEqualTo("1234");
        assertThat(message.getText()).isEqualTo("some message");
        assertThat(message.getReplyToMessageId()).isNull();
    }

    @Test
    void shouldReactWithReply() {
        // given
        when(context.getReplyTo()).thenReturn(321);
        final MessageReaction testObj = MessageReaction.reply("some message");

        // when
        testObj.accept(context);

        // then
        verify(context).execute(methodCaptor.capture());
        final SendMessage message = methodCaptor.getValue();
        assertThat(message.getChatId()).isEqualTo("1234");
        assertThat(message.getText()).isEqualTo("some message");
        assertThat(message.getReplyToMessageId()).isEqualTo(321);
    }
}