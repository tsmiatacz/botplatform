package pl.tsmiatacz.botplatform.behavior.reaction;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;
import pl.tsmiatacz.botplatform.bots.AbstractBot;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class ReloadDataReactionTest {

    @InjectMocks
    private ReloadDataReaction testObj;

    @Mock
    private BehaviorContext context;
    @Mock
    private AbstractBot bot;

    @Test
    void shouldReloadData() {
        // given
        when(context.getBot()).thenReturn(bot);

        // when
        testObj.accept(context);

        // then
        verify(bot).initialize();
    }
}