package pl.tsmiatacz.botplatform.behavior.reaction;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.EntityType;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.MessageEntity;
import org.telegram.telegrambots.meta.api.objects.Update;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;
import pl.tsmiatacz.botplatform.bots.AbstractBot;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class UserMessageReactionTest {

    @InjectMocks
    private UserMessageReaction testObj = UserMessageReaction.of("some message");

    @Mock
    private BehaviorContext context;
    @Mock
    private Update update;
    @Mock
    private Message message;
    @Mock
    private AbstractBot bot;
    @Mock
    private MessageEntity user1mention, user2mention, botMention, adminMention;

    @Captor
    private ArgumentCaptor<SendMessage> methodCaptor;

    @BeforeEach
    void setUp() {
        when(context.getUpdate()).thenReturn(update);
        when(update.getMessage()).thenReturn(message);
        when(context.getBot()).thenReturn(bot);
        when(bot.getBotUsername()).thenReturn("some_bot");
        when(bot.getBotAdmin()).thenReturn("bot_admin");
        when(user1mention.getType()).thenReturn(EntityType.MENTION);
        when(user1mention.getText()).thenReturn("@user1");
        when(user2mention.getType()).thenReturn(EntityType.MENTION);
        when(user2mention.getText()).thenReturn("@user2");
        when(botMention.getType()).thenReturn(EntityType.MENTION);
        when(botMention.getText()).thenReturn("@some_bot");
        when(adminMention.getType()).thenReturn(EntityType.MENTION);
        when(adminMention.getText()).thenReturn("@bot_admin");
        when(context.getChatId()).thenReturn("100");
    }

    @Test
    void shouldReactByAddressingUsers() {
        // given
        when(message.getEntities()).thenReturn(List.of(user1mention, user2mention, botMention, adminMention));

        // when
        testObj.accept(context);

        // then
        verify(context, times(2)).execute(methodCaptor.capture());
        final List<SendMessage> messages = methodCaptor.getAllValues();
        assertThat(messages)
                .extracting(SendMessage::getChatId)
                .containsExactly("100", "100");
        assertThat(messages)
                .extracting(SendMessage::getText)
                .containsExactly("@user1 some message", "@user2 some message");
    }
}