package pl.tsmiatacz.botplatform.behavior.reaction;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.telegram.telegrambots.meta.api.methods.AnswerInlineQuery;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.inlinequery.InlineQuery;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResultArticle;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;
import pl.tsmiatacz.botplatform.data.TextData;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class InlineMessageReactionTest {

    @InjectMocks
    private InlineMessageReaction testObj = InlineMessageReaction.of(this::messageProvider);

    @Mock
    private BehaviorContext context;
    @Mock
    private Update update;
    @Mock
    private InlineQuery inlineQuery;

    @Captor
    private ArgumentCaptor<AnswerInlineQuery> methodCaptor;

    private final TextData text1 = TextData.of(1L, "text1");
    private final TextData text2 = TextData.of(2L, "text2");

    @BeforeEach
    void setUp() {
        when(context.getUpdate()).thenReturn(update);
        when(update.getInlineQuery()).thenReturn(inlineQuery);
        when(inlineQuery.getId()).thenReturn("queryId");
    }

    @Test
    void shouldAddInlineMessagesForValidQuery() {
        // given
        when(inlineQuery.getQuery()).thenReturn("my query");

        // when
        testObj.accept(context);

        // then
        verify(context).execute(methodCaptor.capture());
        final AnswerInlineQuery answer = methodCaptor.getValue();
        assertThat(answer.getResults())
                .extracting(InlineQueryResultArticle.class::cast)
                .extracting(InlineQueryResultArticle::getId)
                .containsExactly("1", "2");
        assertThat(answer.getResults())
                .extracting(InlineQueryResultArticle.class::cast)
                .extracting(InlineQueryResultArticle::getDescription)
                .containsExactly("text1", "text2");
    }

    @Test
    void shouldAddNoInlineMessagesForInalidQuery() {
        // given
        when(inlineQuery.getQuery()).thenReturn("other query");

        // when
        testObj.accept(context);

        // then
        verify(context).execute(methodCaptor.capture());
        final AnswerInlineQuery answer = methodCaptor.getValue();
        assertThat(answer.getResults()).isEmpty();
    }

    private List<TextData> messageProvider(final String query) {
        if (query.equals("my query")) {
            return List.of(text1, text2);
        }
        return List.of();
    }
}