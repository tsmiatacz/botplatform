package pl.tsmiatacz.botplatform.behavior.reaction;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;
import pl.tsmiatacz.botplatform.behavior.BehaviorException;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class SayInGroupReactionTest {

    @InjectMocks
    private SayInGroupReaction testObj;

    @Mock
    private BehaviorContext context;

    @Captor
    private ArgumentCaptor<SendMessage> methodCaptor;

    @Test
    void shouldThrowExceptionOnInvalidArguments() {
        // given
        when(context.getArguments()).thenReturn(List.of("only argument"));

        // when
        assertThatThrownBy(() -> testObj.accept(context))

                // then
                .isInstanceOf(BehaviorException.class);
    }

    @Test
    void shouldSendMessageToGivenGroup() {
        // given
        when(context.getArguments()).thenReturn(List.of("-333", "my message"));
        when(context.argument(0)).thenReturn("-333");
        when(context.argument(1)).thenReturn("my message");

        // when
        testObj.accept(context);

        // then
        verify(context).execute(methodCaptor.capture());
        final SendMessage message = methodCaptor.getValue();
        assertThat(message.getChatId()).isEqualTo("-333");
        assertThat(message.getText()).isEqualTo("my message");
    }
}