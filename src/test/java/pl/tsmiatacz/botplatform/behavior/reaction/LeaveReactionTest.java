package pl.tsmiatacz.botplatform.behavior.reaction;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.telegram.telegrambots.meta.api.methods.groupadministration.LeaveChat;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class LeaveReactionTest {

    @InjectMocks
    private LeaveReaction testObj;

    @Mock
    private BehaviorContext context;

    @Captor
    private ArgumentCaptor<LeaveChat> methodCaptor;

    @Test
    void shouldLeaveCurrentGroup() {
        // given
        when(context.getArguments()).thenReturn(List.of());
        when(context.getChatId()).thenReturn("555");

        // when
        testObj.accept(context);

        // then
        verify(context).execute(methodCaptor.capture());
        final LeaveChat leave = methodCaptor.getValue();
        assertThat(leave.getChatId()).isEqualTo("555");
    }

    @Test
    void shouldLeaveGivenGroup() {
        // given
        when(context.getArguments()).thenReturn(List.of("777"));
        when(context.getChatId()).thenReturn("555");

        // when
        testObj.accept(context);

        // then
        verify(context).execute(methodCaptor.capture());
        final LeaveChat leave = methodCaptor.getValue();
        assertThat(leave.getChatId()).isEqualTo("777");
    }
}