package pl.tsmiatacz.botplatform.behavior.reaction;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.User;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;
import pl.tsmiatacz.botplatform.bots.AbstractBot;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class BotInfoReactionTest {

    private static final String TEMPLATE = "I'm ${firstName} ${lastName} (${userName}). My id is ${userId}. Chat id is ${chatId}.";

    @InjectMocks
    private BotInfoReaction testObj = BotInfoReaction.of(TEMPLATE);

    @Mock
    private BehaviorContext context;
    @Mock
    private AbstractBot bot;
    @Mock
    private User user;

    @Captor
    private ArgumentCaptor<SendMessage> methodCaptor;


    @BeforeEach
    void setUp() {
        when(context.getChatId()).thenReturn("123");
        when(context.getBot()).thenReturn(bot);
        when(bot.getUser()).thenReturn(user);
        when(user.getFirstName()).thenReturn("Testowy");
        when(user.getLastName()).thenReturn("Tester");
        when(user.getUserName()).thenReturn("test_bot");
        when(user.getId()).thenReturn(321L);
    }

    @Test
    void shouldSendInfoMessage() {
        // given

        // when
        testObj.accept(context);

        // then
        verify(context).execute(methodCaptor.capture());
        final SendMessage message = methodCaptor.getValue();
        assertThat(message.getChatId()).isEqualTo("123");
        assertThat(message.getText()).isEqualTo("I'm Testowy Tester (test_bot). My id is 321. Chat id is 123.");
    }
}