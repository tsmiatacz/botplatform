package pl.tsmiatacz.botplatform.behavior;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import pl.tsmiatacz.botplatform.behavior.condition.Condition;
import pl.tsmiatacz.botplatform.behavior.handler.Handler;
import pl.tsmiatacz.botplatform.behavior.reaction.Reaction;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class BehaviorTest {

    @InjectMocks
    private Behavior testObj;

    @Mock
    private Condition condition;
    @Mock
    private Reaction reaction;
    @Mock
    private Handler exceptionHandler;
    @Mock
    private BehaviorContext context;
    @Mock
    private RuntimeException exception;

    @Test
    void shouldNotReactWhenConditionNotMet() {
        // given
        when(condition.test(context)).thenReturn(false);

        // when
        testObj.evaluate(context);

        // then
        verify(reaction, never()).accept(context);
    }

    @Test
    void shouldReactWhenConditionMet() {
        // given
        when(condition.test(context)).thenReturn(true);

        // when
        testObj.evaluate(context);

        // then
        verify(reaction).accept(context);
    }

    @Test
    void shouldHandleException() {
        // given
        when(condition.test(context)).thenReturn(true);
        doThrow(exception).when(reaction).accept(context);

        // when
        testObj.evaluate(context);

        // then
        verify(reaction).accept(context);
        verify(exceptionHandler).accept(exception, context);
    }
}