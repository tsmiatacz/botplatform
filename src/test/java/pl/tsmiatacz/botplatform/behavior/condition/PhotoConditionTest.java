package pl.tsmiatacz.botplatform.behavior.condition;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class PhotoConditionTest {

    @InjectMocks
    private PhotoCondition testObj;

    @Mock
    private BehaviorContext contextMock;
    @Mock
    private Update updateMock;
    @Mock
    private Message messageMock;

    @BeforeEach
    void setUp() {
        when(contextMock.getUpdate()).thenReturn(updateMock);
    }

    @Test
    void shouldReturnFalseWhenNoMessage() {
        // given
        when(updateMock.hasMessage()).thenReturn(false);

        // when
        final boolean result = testObj.check(contextMock);

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldReturnFalseWhenNoPhotoInMessage() {
        // given
        when(updateMock.hasMessage()).thenReturn(true);
        when(updateMock.getMessage()).thenReturn(messageMock);
        when(messageMock.hasPhoto()).thenReturn(false);

        // when
        final boolean result = testObj.check(contextMock);

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldReturnTrueWhenPhotoInMessage() {
        // given
        when(updateMock.hasMessage()).thenReturn(true);
        when(updateMock.getMessage()).thenReturn(messageMock);
        when(messageMock.hasPhoto()).thenReturn(true);

        // when
        final boolean result = testObj.check(contextMock);

        // then
        assertThat(result).isTrue();
    }
}