package pl.tsmiatacz.botplatform.behavior.condition;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.telegram.telegrambots.meta.api.objects.*;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;
import pl.tsmiatacz.botplatform.bots.AbstractBot;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class MessageConditionTest {

    @Mock
    private BehaviorContext context;
    @Mock
    private Update update;
    @Mock
    private AbstractBot bot;
    @Mock
    private Message message;
    @Mock
    private MessageEntity entity;

    @BeforeEach
    void setUp() {
        when(context.getUpdate()).thenReturn(update);
        when(context.getBot()).thenReturn(bot);
        when(bot.getBotUsername()).thenReturn("some_bot");
        when(entity.getType()).thenReturn(EntityType.MENTION);
    }

    @Nested
    class EndsWithMessageConditionTest {

        @InjectMocks
        private MessageCondition testObj = MessageCondition.endsWith("my request");

        @Test
        void shouldReturnFalseWhenNotMessage() {
            // given
            when(update.hasMessage()).thenReturn(false);

            // when
            final boolean result = testObj.test(context);

            // then
            assertThat(result).isFalse();
        }

        @Test
        void shouldReturnFalseWhenInvalidMessageInUserChat() {
            // given
            when(update.hasMessage()).thenReturn(true);
            when(context.isUserChat()).thenReturn(true);
            when(update.getMessage()).thenReturn(message);
            when(message.getText()).thenReturn("my request but ending with something");

            // when
            final boolean result = testObj.test(context);

            // then
            assertThat(result).isFalse();
        }

        @Test
        void shouldReturnTrueWhenValidMessageInUserChat() {
            // given
            when(update.hasMessage()).thenReturn(true);
            when(context.isUserChat()).thenReturn(true);
            when(update.getMessage()).thenReturn(message);
            when(message.getText()).thenReturn("something about my request");

            // when
            final boolean result = testObj.test(context);

            // then
            assertThat(result).isTrue();
        }

        @Test
        void shouldReturnFalseWhenBotNotMentionedInGroupChat() {
            // given
            when(update.hasMessage()).thenReturn(true);
            when(context.isUserChat()).thenReturn(false);
            when(update.getMessage()).thenReturn(message);
            when(message.getEntities()).thenReturn(List.of());
            when(message.getText()).thenReturn("my request");

            // when
            final boolean result = testObj.test(context);

            // then
            assertThat(result).isFalse();
        }

        @Test
        void shouldReturnTrueWhenBotMentionedInGroupChat() {
            // given
            when(update.hasMessage()).thenReturn(true);
            when(context.isUserChat()).thenReturn(true);
            when(update.getMessage()).thenReturn(message);
            when(message.getEntities()).thenReturn(List.of(entity));
            when(entity.getText()).thenReturn("@some_bot");
            when(message.getText()).thenReturn("@some_bot my request");

            // when
            final boolean result = testObj.test(context);

            // then
            assertThat(result).isTrue();
        }
    }

    @Nested
    class ContainsMessageConditionTest {

        @InjectMocks
        private MessageCondition testObj = MessageCondition.contains("my request");

        @Test
        void shouldReturnFalseWhenNotMessage() {
            // given
            when(update.hasMessage()).thenReturn(false);

            // when
            final boolean result = testObj.test(context);

            // then
            assertThat(result).isFalse();
        }

        @Test
        void shouldReturnFalseWhenInvalidMessageInUserChat() {
            // given
            when(update.hasMessage()).thenReturn(true);
            when(context.isUserChat()).thenReturn(true);
            when(update.getMessage()).thenReturn(message);
            when(message.getText()).thenReturn("invalid request");

            // when
            final boolean result = testObj.test(context);

            // then
            assertThat(result).isFalse();
        }

        @Test
        void shouldReturnTrueWhenValidMessageInUserChat() {
            // given
            when(update.hasMessage()).thenReturn(true);
            when(context.isUserChat()).thenReturn(true);
            when(update.getMessage()).thenReturn(message);
            when(message.getText()).thenReturn("this is my request surrounded with some text");

            // when
            final boolean result = testObj.test(context);

            // then
            assertThat(result).isTrue();
        }

        @Test
        void shouldReturnFalseWhenBotNotMentionedInGroupChat() {
            // given
            when(update.hasMessage()).thenReturn(true);
            when(context.isUserChat()).thenReturn(false);
            when(update.getMessage()).thenReturn(message);
            when(message.getEntities()).thenReturn(List.of());
            when(message.getText()).thenReturn("my request");

            // when
            final boolean result = testObj.test(context);

            // then
            assertThat(result).isFalse();
        }

        @Test
        void shouldReturnTrueWhenBotMentionedInGroupChat() {
            // given
            when(update.hasMessage()).thenReturn(true);
            when(context.isUserChat()).thenReturn(true);
            when(update.getMessage()).thenReturn(message);
            when(message.getEntities()).thenReturn(List.of(entity));
            when(entity.getText()).thenReturn("@some_bot");
            when(message.getText()).thenReturn("@some_bot my request");

            // when
            final boolean result = testObj.test(context);

            // then
            assertThat(result).isTrue();
        }
    }

}