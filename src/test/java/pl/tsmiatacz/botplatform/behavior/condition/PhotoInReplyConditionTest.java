package pl.tsmiatacz.botplatform.behavior.condition;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class PhotoInReplyConditionTest {

    @InjectMocks
    private PhotoInReplyCondition testObj;

    @Mock
    private BehaviorContext contextMock;
    @Mock
    private Update updateMock;
    @Mock
    private Message messageMock;
    @Mock
    private Message replyMock;

    @BeforeEach
    void setUp() {
        when(contextMock.getUpdate()).thenReturn(updateMock);
    }

    @Test
    void shouldReturnFalseWhenNoMessage() {
        // given
        when(updateMock.hasMessage()).thenReturn(false);

        // when
        final boolean result = testObj.check(contextMock);

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldReturnFalseWhenNoReplyMessage() {
        // given
        when(updateMock.hasMessage()).thenReturn(true);
        when(updateMock.getMessage()).thenReturn(messageMock);
        when(messageMock.isReply()).thenReturn(false);


        // when
        final boolean result = testObj.check(contextMock);

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldReturnFalseWhenNoPhotoInReply() {
        // given
        when(updateMock.hasMessage()).thenReturn(true);
        when(updateMock.getMessage()).thenReturn(messageMock);
        when(messageMock.isReply()).thenReturn(true);
        when(messageMock.getReplyToMessage()).thenReturn(replyMock);
        when(replyMock.hasPhoto()).thenReturn(false);

        // when
        final boolean result = testObj.check(contextMock);

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldReturnTrueWhenPhotoInReply() {
        // given
        when(updateMock.hasMessage()).thenReturn(true);
        when(updateMock.getMessage()).thenReturn(messageMock);
        when(messageMock.isReply()).thenReturn(true);
        when(messageMock.getReplyToMessage()).thenReturn(replyMock);
        when(replyMock.hasPhoto()).thenReturn(true);

        // when
        final boolean result = testObj.check(contextMock);

        // then
        assertThat(result).isTrue();
    }
}