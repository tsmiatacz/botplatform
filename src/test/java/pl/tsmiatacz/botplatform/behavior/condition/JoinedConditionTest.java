package pl.tsmiatacz.botplatform.behavior.condition;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;
import pl.tsmiatacz.botplatform.bots.AbstractBot;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class JoinedConditionTest {

    @InjectMocks
    private JoinedCondition testObj = JoinedCondition.user("some_user");

    @Mock
    private BehaviorContext context;
    @Mock
    private Update update;
    @Mock
    private AbstractBot bot;
    @Mock
    private Message message;
    @Mock
    private User user;

    @BeforeEach
    void setUp() {
        when(context.getUpdate()).thenReturn(update);
    }

    @Test
    void shouldReturnFalseWhenNotMessage() {
        // given
        when(update.hasMessage()).thenReturn(false);

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldReturnFalseWhenUserChat() {
        // given
        when(update.hasMessage()).thenReturn(true);
        when(context.isUserChat()).thenReturn(true);

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldReturnFalseNoNewChatMembers() {
        // given
        when(update.hasMessage()).thenReturn(true);
        when(context.isUserChat()).thenReturn(false);
        when(update.getMessage()).thenReturn(message);
        when(message.getNewChatMembers()).thenReturn(List.of());

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldReturnFalseWhenOtherUserJoined() {
        // given
        when(update.hasMessage()).thenReturn(true);
        when(context.isUserChat()).thenReturn(false);
        when(update.getMessage()).thenReturn(message);
        when(message.getNewChatMembers()).thenReturn(List.of(user));
        when(user.getUserName()).thenReturn("other_user");

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldReturnTrueWhenGivenUserJoined() {
        // given
        when(update.hasMessage()).thenReturn(true);
        when(context.isUserChat()).thenReturn(false);
        when(update.getMessage()).thenReturn(message);
        when(message.getNewChatMembers()).thenReturn(List.of(user));
        when(user.getUserName()).thenReturn("some_user");

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isTrue();
    }

    @Test
    void shouldReturnTrueWhenSelfJoined() {
        // given
        final JoinedCondition selfCondition = JoinedCondition.self();
        when(context.getBot()).thenReturn(bot);
        when(bot.getBotUsername()).thenReturn("some_bot");
        when(update.hasMessage()).thenReturn(true);
        when(context.isUserChat()).thenReturn(false);
        when(update.getMessage()).thenReturn(message);
        when(message.getNewChatMembers()).thenReturn(List.of(user));
        when(user.getUserName()).thenReturn("some_bot");

        // when
        final boolean result = selfCondition.test(context);

        // then
        assertThat(result).isTrue();
    }
}