package pl.tsmiatacz.botplatform.behavior.condition;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class IsUserChatConditionTest {

    @InjectMocks
    private IsUserChatCondition testObj;

    @Mock
    private BehaviorContext context;

    @Test
    void shouldReturnFalseWhenNotUserChat() {
        // given
        when(context.isUserChat()).thenReturn(false);

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldReturnTrueWhenUserChat() {
        // given
        when(context.isUserChat()).thenReturn(true);

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isTrue();
    }
}