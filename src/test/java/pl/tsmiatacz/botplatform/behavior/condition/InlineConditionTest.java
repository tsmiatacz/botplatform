package pl.tsmiatacz.botplatform.behavior.condition;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.telegram.telegrambots.meta.api.objects.Update;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class InlineConditionTest {

    @InjectMocks
    private InlineCondition testObj;

    @Mock
    private BehaviorContext context;
    @Mock
    private Update update;

    @BeforeEach
    void setUp() {
        when(context.getUpdate()).thenReturn(update);
    }

    @Test
    void shouldReturnFalseWhenUpdateHasNoInlineQuery() {
        // given
        when(update.hasInlineQuery()).thenReturn(false);

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldReturnTrueWhenUpdateHasInlineQuery() {
        // given
        when(update.hasInlineQuery()).thenReturn(true);

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isTrue();
    }
}