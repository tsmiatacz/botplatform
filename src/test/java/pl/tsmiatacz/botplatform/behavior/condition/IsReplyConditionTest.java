package pl.tsmiatacz.botplatform.behavior.condition;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class IsReplyConditionTest {

    @InjectMocks
    private IsReplyCondition testObj;

    @Mock
    private BehaviorContext context;
    @Mock
    private Update update;
    @Mock
    private Message message;

    @BeforeEach
    void setUp() {
        when(context.getUpdate()).thenReturn(update);
    }

    @Test
    void shouldReturnFalseWhenNotMessage() {
        // given
        when(update.hasMessage()).thenReturn(false);

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldReturnFalseWhenNotReply() {
        // given
        when(update.hasMessage()).thenReturn(true);
        when(update.getMessage()).thenReturn(message);
        when(message.isReply()).thenReturn(false);

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldReturnTrueWhenReply() {
        // given
        when(update.hasMessage()).thenReturn(true);
        when(update.getMessage()).thenReturn(message);
        when(message.isReply()).thenReturn(true);

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isTrue();
    }
}