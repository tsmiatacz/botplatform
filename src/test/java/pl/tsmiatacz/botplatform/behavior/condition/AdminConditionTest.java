package pl.tsmiatacz.botplatform.behavior.condition;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;
import pl.tsmiatacz.botplatform.bots.AbstractBot;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class AdminConditionTest {

    @InjectMocks
    private AdminCondition testObj;

    @Mock
    private BehaviorContext context;
    @Mock
    private Update update;
    @Mock
    private AbstractBot bot;
    @Mock
    private User user;

    @BeforeEach
    void setUp() {
        when(context.getUpdate()).thenReturn(update);
    }

    @Test
    void shouldReturnFalseWhenUpdateHasNoMessage() {
        // given
        when(update.hasMessage()).thenReturn(false);

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldReturnFalseWhenMessageNotFromAdmin() {
        // given
        when(update.hasMessage()).thenReturn(true);
        when(context.getBot()).thenReturn(bot);
        when(bot.getBotAdmin()).thenReturn("admin");
        when(context.getFrom()).thenReturn(user);
        when(user.getUserName()).thenReturn("notAdmin");

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldReturnTrueWhenMessageFromFromAdmin() {
        // given
        when(update.hasMessage()).thenReturn(true);
        when(context.getBot()).thenReturn(bot);
        when(bot.getBotAdmin()).thenReturn("admin");
        when(context.getFrom()).thenReturn(user);
        when(user.getUserName()).thenReturn("admin");

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isTrue();
    }
}