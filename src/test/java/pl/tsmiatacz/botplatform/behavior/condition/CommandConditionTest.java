package pl.tsmiatacz.botplatform.behavior.condition;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.telegram.telegrambots.meta.api.objects.EntityType;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.MessageEntity;
import org.telegram.telegrambots.meta.api.objects.Update;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;
import pl.tsmiatacz.botplatform.bots.AbstractBot;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class CommandConditionTest {

    @InjectMocks
    private CommandCondition testObj = CommandCondition.of("/command");

    @Mock
    private BehaviorContext context;
    @Mock
    private AbstractBot bot;
    @Mock
    private Update update;
    @Mock
    private Message message;
    @Mock
    private MessageEntity entity;

    @BeforeEach
    void setUp() {
        when(context.getUpdate()).thenReturn(update);
        when(context.getBot()).thenReturn(bot);
        when(bot.getBotUsername()).thenReturn("some_bot");
        when(entity.getType()).thenReturn(EntityType.BOTCOMMAND);
    }

    @Test
    void shouldReturnFalseWhenUpdateHasNoMessage() {
        // given
        when(update.hasMessage()).thenReturn(false);

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldReturnFalseWhenUpdateHasNoBotCommands() {
        // given
        when(update.hasMessage()).thenReturn(true);
        when(update.getMessage()).thenReturn(message);
        when(message.getEntities()).thenReturn(List.of());

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldReturnFalseWhenInvalidCommandInUserChat() {
        // given
        when(update.hasMessage()).thenReturn(true);
        when(update.getMessage()).thenReturn(message);
        when(context.isUserChat()).thenReturn(true);
        when(message.getEntities()).thenReturn(List.of(entity));
        when(entity.getText()).thenReturn("/otherCommand");

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldReturnTrueWhenValidCommandInUserChat() {
        // given
        when(update.hasMessage()).thenReturn(true);
        when(update.getMessage()).thenReturn(message);
        when(context.isUserChat()).thenReturn(true);
        when(message.getEntities()).thenReturn(List.of(entity));
        when(entity.getText()).thenReturn("/command");

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isTrue();
    }

    @Test
    void shouldReturnFalseWhenBotNotMentionedInGroupChat() {
        // given
        when(update.hasMessage()).thenReturn(true);
        when(update.getMessage()).thenReturn(message);
        when(context.isUserChat()).thenReturn(false);
        when(message.getEntities()).thenReturn(List.of(entity));
        when(entity.getText()).thenReturn("/command");

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isFalse();
    }

    @Test
    void shouldReturnTrueWhenBotMentionedInGroupChat() {
        // given
        when(update.hasMessage()).thenReturn(true);
        when(update.getMessage()).thenReturn(message);
        when(context.isUserChat()).thenReturn(false);
        when(message.getEntities()).thenReturn(List.of(entity));
        when(entity.getText()).thenReturn("/command@some_bot");

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isTrue();
    }

}