package pl.tsmiatacz.botplatform.behavior.condition;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class AlwaysConditionTest {

    @InjectMocks
    private AlwaysCondition testObj;

    @Mock
    private BehaviorContext context;

    @Test
    void shouldAlwaysBeTrue() {
        // given

        // when
        final boolean result = testObj.test(context);

        // then
        assertThat(result).isTrue();
    }
}