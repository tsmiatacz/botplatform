package pl.tsmiatacz.botplatform.bots.impl;

import com.google.inject.Injector;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import pl.tsmiatacz.botplatform.bots.AbstractBot;
import pl.tsmiatacz.botplatform.service.ConfigurationService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class DefaultBotFactoryTest {

    @InjectMocks
    private DefaultBotFactory testObj;

    @Mock
    private ConfigurationService configurationService;
    @Mock
    private Injector injector;

    @Mock
    private AbstractBot botMock;

    @BeforeEach
    void setUp() {
        when(configurationService.getBotsPackage()).thenReturn("pl.tsmiatacz.botplatform.bots");
    }

    @Test
    void shouldCreateBot() {
        // given
        when(injector.getInstance(AbstractBot.class)).thenReturn(botMock);

        // when
        final AbstractBot result = testObj.create("AbstractBot");

        // then
        assertThat(result).isEqualTo(botMock);
    }

    @Test
    void shouldThrowExceptionOnFail() {
        // given
        doThrow(RuntimeException.class).when(injector).getInstance(AbstractBot.class);

        // when
        assertThatThrownBy(() -> testObj.create("AbstractBot"))

                // then
                .isInstanceOf(IllegalStateException.class);
    }
}