package pl.tsmiatacz.botplatform.service;

import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public interface InitializerService {

    /**
     * Method responsible for creating and registring bots in Telegram API
     */
    void initialize() throws TelegramApiException;
}
