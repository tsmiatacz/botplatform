package pl.tsmiatacz.botplatform.service;

import java.util.List;

public interface ConfigurationService {

    String get(String code);

    List<String> getEnabledBots();

    String getGroupId(String code);

    default String getLogGroupId() {
        return getGroupId("log");
    }

    String getBotsPackage();
    String getBotName(String type);
    String getBotKey(String type);
    String getBotAdmin(String type);

    void reload();
}
