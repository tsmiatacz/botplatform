package pl.tsmiatacz.botplatform.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;
import pl.tsmiatacz.botplatform.bots.AbstractBot;
import pl.tsmiatacz.botplatform.bots.BotFactory;
import pl.tsmiatacz.botplatform.service.ConfigurationService;
import pl.tsmiatacz.botplatform.service.InitializerService;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DefaultInitializerService implements InitializerService {

    private static final Logger LOG = LogManager.getLogger(DefaultInitializerService.class);

    private final ConfigurationService configurationService;
    private final BotFactory botFactory;

    @Inject
    private DefaultInitializerService(final ConfigurationService configurationService, final BotFactory botFactory) {
        this.configurationService = configurationService;
        this.botFactory = botFactory;
    }

    @Override
    public final void initialize() throws TelegramApiException {
        final TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
        configurationService.getEnabledBots().stream()
                .map(botFactory::create)
                .forEach(bot -> registerBot(telegramBotsApi, bot));
        LOG.info("Bot api initialized");
    }

    private void registerBot(final TelegramBotsApi telegramBotsApi, final AbstractBot bot) {
        try {
            telegramBotsApi.registerBot(bot);
        } catch (final TelegramApiException e) {
            throw new IllegalStateException("Error while registering bot " + bot.getBotUsername(), e);
        }
    }
}
