package pl.tsmiatacz.botplatform.service.impl;

import org.apache.commons.lang3.StringUtils;
import pl.tsmiatacz.botplatform.service.ConfigurationService;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

@Singleton
public class DefaultConfigurationService implements ConfigurationService {

    private final String profile;
    private Properties properties;

    @Inject
    public DefaultConfigurationService(final String profile) {
        this.profile = profile;
        reload();
    }

    @Override
    public String get(final String code) {
        return getProperty(code);
    }

    @Override
    public List<String> getEnabledBots() {
        final String enabledBots = getProperty("bots.enabled");
        return Arrays.asList(StringUtils.split(enabledBots, ","));
    }

    @Override
    public String getGroupId(final String code) {
        return getProperty("group.id." + code);
    }

    @Override
    public String getBotsPackage() {
        return getProperty("bots.package");
    }

    @Override
    public String getBotName(final String type) {
        return getBotProperty("name", type);
    }

    @Override
    public String getBotKey(final String type) {
        return getBotProperty("key", type);
    }

    @Override
    public String getBotAdmin(final String type) {
        return getBotProperty("admin", type);
    }

    @Override
    public void reload() {
        try (InputStream input = getClass().getResourceAsStream(String.format("/%s.properties", profile))){
            properties = new Properties();
            properties.load(new InputStreamReader(input, StandardCharsets.UTF_8));
        } catch (final IOException e) {
            throw new IllegalStateException("Cannot load properties");
        }
    }

    private String getProperty(final String code) {
        return Optional.ofNullable(properties.getProperty(code))
                .orElseThrow(() -> new IllegalStateException("Cannot find property: " + code));
    }

    private String getBotProperty(final String code, final String type) {
        return getProperty(String.format("bot.%s.%s", type, code));
    }
}
