package pl.tsmiatacz.botplatform.service.impl;

import pl.tsmiatacz.botplatform.service.TimeService;

import javax.inject.Singleton;
import java.time.LocalDateTime;
import java.util.Random;

@Singleton
public class DefaultTimeService implements TimeService {

    private Random random = new Random();

    @Override
    public LocalDateTime randomTime() {
        final long seconds = random.nextInt(36900) + 3600L;
        return now().plusSeconds(seconds);
    }
}
