package pl.tsmiatacz.botplatform.service;

import java.time.LocalDateTime;

public interface TimeService {

    default LocalDateTime now() {
        return LocalDateTime.now();
    }

    LocalDateTime randomTime();
}
