package pl.tsmiatacz.botplatform.bots.impl;

import com.google.inject.Injector;
import pl.tsmiatacz.botplatform.bots.AbstractBot;
import pl.tsmiatacz.botplatform.bots.BotFactory;
import pl.tsmiatacz.botplatform.service.ConfigurationService;

import javax.inject.Inject;
import java.util.Objects;

public class DefaultBotFactory implements BotFactory {

    private final ConfigurationService configurationService;
    private final Injector injector;

    @Inject
    public DefaultBotFactory(final ConfigurationService configurationService,
                             final Injector injector) {
        this.configurationService = configurationService;
        this.injector = injector;
    }

    @Override
    public AbstractBot create(final String type) {
        Objects.requireNonNull(type, "type cannot be null");
        try {
            final String botsPackage = configurationService.getBotsPackage();
            return (AbstractBot) injector.getInstance(Class.forName(botsPackage + "." + type));
        } catch (final Exception e) {
            throw new IllegalStateException("Cannot create bot " + type, e);
        }
    }
}
