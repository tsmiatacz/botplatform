package pl.tsmiatacz.botplatform.bots;

import com.google.common.collect.Lists;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.GetMe;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import pl.tsmiatacz.botplatform.behavior.Behavior;
import pl.tsmiatacz.botplatform.behavior.StaticBehaviorContext;
import pl.tsmiatacz.botplatform.behavior.UpdateBehaviorContext;
import pl.tsmiatacz.botplatform.service.ConfigurationService;

import java.util.Arrays;
import java.util.List;

public abstract class AbstractBot extends TelegramLongPollingBot {

    protected final ConfigurationService configurationService;

    private List<Behavior> behaviors = Lists.newArrayList();

    public AbstractBot(final ConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    /**
     * Override this method to add behaviors
     */
    public void initialize() {
        configurationService.reload();
        behaviors = Lists.newArrayList();
    }

    protected void addBehaviors(final Behavior... behavior) {
        behaviors.addAll(Arrays.asList(behavior));
    }

    @Override
    public final void onUpdateReceived(final Update update) {
        final var behaviorContext = UpdateBehaviorContext.of(this, update);
        behaviors.forEach(behavior -> behavior.evaluate(behaviorContext));
        onUpdate(update);
    }

    /**
     * Implement this method when you need custom functionality that couldn't be implemented as Behavior
     */
    protected void onUpdate(final Update update) {
        // intentionally left blank
    }

    @Override
    public String getBotToken() {
        return configurationService.getBotKey(getBotType());
    }

    @Override
    public String getBotUsername() {
        return configurationService.getBotName(getBotType());
    }

    public String getBotAdmin() {
        return configurationService.getBotAdmin(getBotType());
    }

    public User getUser() {
        var staticContext = StaticBehaviorContext.builder(this).build();
        return staticContext.execute(new GetMe());
    }

    public String getBotType() {
        return this.getClass().getSimpleName();
    }
}
