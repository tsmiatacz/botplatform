package pl.tsmiatacz.botplatform.bots;

public interface BotFactory {

    /**
     * Creates a new bot instance for given type
     * @param type not null
     *             bot type, eg. from properties
     * @return not null
     */
    AbstractBot create(String type);
}
