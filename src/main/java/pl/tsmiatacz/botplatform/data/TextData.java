package pl.tsmiatacz.botplatform.data;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

public class TextData implements Serializable {

    private String id;
    private String text;

    private TextData(final String id, final String text) {
        this.id = id;
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        final TextData textData = (TextData) o;

        return new EqualsBuilder()
                .append(id, textData.id)
                .append(text, textData.text)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .append(text)
                .toHashCode();
    }

    public static TextData of(final long id, final String text) {
        return new TextData(String.valueOf(id), text);
    }
}
