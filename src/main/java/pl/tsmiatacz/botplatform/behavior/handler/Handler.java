package pl.tsmiatacz.botplatform.behavior.handler;

import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import java.util.Objects;
import java.util.function.BiConsumer;

public abstract class Handler implements BiConsumer<Exception, BehaviorContext> {

    /**
     * @param e not null
     * @param context not null
     */
    protected abstract void handle(Exception e, BehaviorContext context);

    @Override
    public final void accept(Exception e, BehaviorContext context) {
        Objects.requireNonNull(e, "exception cannot be null");
        Objects.requireNonNull(context, "context cannot be null");
        handle(e, context);
    }
}
