package pl.tsmiatacz.botplatform.behavior.handler;

import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import pl.tsmiatacz.botplatform.Application;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;
import pl.tsmiatacz.botplatform.service.ConfigurationService;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;

/**
 * This exception handler will write exception stack trace in designated group
 */
public class GroupLogExceptionHandler extends Handler {

    private static final GroupLogExceptionHandler INSTANCE = new GroupLogExceptionHandler();

    private final ConfigurationService configurationService;

    private GroupLogExceptionHandler() {
        this.configurationService = Application.getInjector()
                .getInstance(ConfigurationService.class);
    }

    @Override
    protected void handle(final Exception e, final BehaviorContext context) {
        final var message =
                SendMessage.builder()
                        .chatId(configurationService.getLogGroupId())
                        .text(createMessage(e))
                        .parseMode(ParseMode.MARKDOWN)
                        .build();

        context.execute(message);
    }

    private String createMessage(final Exception e) {
        return message(e) + "\n" +
                "```\n" +
                stackTrace(e) +
                "\n```";
    }

    private String message(final Exception e) {
        return Optional.ofNullable(e.getMessage())
                .orElse("Exception occured");
    }

    private String stackTrace(final Exception e) {
        final var sw = new StringWriter();
        final var pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        return sw.toString();
    }

    public static GroupLogExceptionHandler of() {
        return INSTANCE;
    }
}
