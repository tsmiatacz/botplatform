package pl.tsmiatacz.botplatform.behavior;

import com.google.common.collect.Lists;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import pl.tsmiatacz.botplatform.bots.AbstractBot;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Chat update context. Used in most cases.
 */
public class UpdateBehaviorContext implements BehaviorContext {

    private static final Pattern ARGS_PATTERN = Pattern.compile("\"(.*?)\"");
    private final AbstractBot bot;
    private final Update update;

    private UpdateBehaviorContext(final AbstractBot bot, final Update update) {
        this.bot = bot;
        this.update = update;
    }

    @Override
    public AbstractBot getBot() {
        return bot;
    }

    @Override
    public Update getUpdate() {
        return update;
    }

    @Override
    public String getChatId() {
        if (update.hasMessage()) {
            return Long.toString(update.getMessage().getChatId());
        }
        return null;
    }

    @Override
    public Integer getReplyTo() {
        if (update.hasMessage()) {
            if (update.getMessage().isReply()) {
                return update.getMessage().getReplyToMessage().getMessageId();
            }
            return update.getMessage().getMessageId();
        }
        return null;
    }

    @Override
    public boolean isUserChat() {
        if (update.hasMessage()) {
            return update.getMessage().getChat().isUserChat();
        }
        return false;
    }

    /**
     * @return list of arguments from a message
     *  Every text in quotes is treated as an argument, eg.:
     *  some text and then arguments "arg1" "arg2"
     */
    @Override
    public List<String> getArguments() {
        if (!update.hasMessage()) {
            return Collections.emptyList();
        }
        final Matcher matcher = ARGS_PATTERN.matcher(update.getMessage().getText());
        final List<String> args = Lists.newArrayList();
        while (matcher.find()) {
            args.add(matcher.group(1));
        }
        return args;
    }

    @Override
    public User getFrom() {
        if (update.hasMessage()) {
            return update.getMessage().getFrom();
        }
        if (update.hasCallbackQuery()) {
            return update.getCallbackQuery().getFrom();
        }
        return null;
    }

    /**
     * @param bot not null
     * @param update not null
     */
    public static UpdateBehaviorContext of(final AbstractBot bot, final Update update) {
        Objects.requireNonNull(bot, "bot cannot be null");
        Objects.requireNonNull(update, "update cannot be null");

        return new UpdateBehaviorContext(bot, update);
    }
}
