package pl.tsmiatacz.botplatform.behavior.condition;

import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

/**
 * Checks if update comes from user chat (ie. not group, supergroup, channel)
 */
public class IsUserChatCondition extends Condition {

    private static final IsUserChatCondition INSTANCE = new IsUserChatCondition();

    private IsUserChatCondition() {
        // intentionally left blank
    }

    @Override
    protected boolean check(final BehaviorContext context) {
        return context.isUserChat();
    }

    public static IsUserChatCondition of() {
        return INSTANCE;
    }
}
