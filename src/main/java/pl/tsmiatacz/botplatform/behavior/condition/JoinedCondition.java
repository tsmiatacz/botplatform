package pl.tsmiatacz.botplatform.behavior.condition;

import org.telegram.telegrambots.meta.api.objects.User;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import java.util.Collections;
import java.util.Optional;

/**
 * Checks if given user joined chat in this update.
 */
public class JoinedCondition extends Condition {

    private String username;

    private JoinedCondition(final String username) {
        this.username = username;
    }

    @Override
    protected boolean check(final BehaviorContext context) {
        if (!context.getUpdate().hasMessage()) {
            return false;
        }

        return !context.isUserChat()
                && Optional.ofNullable(context.getUpdate().getMessage().getNewChatMembers())
                .orElseGet(Collections::emptyList).stream()
                .map(User::getUserName)
                .anyMatch(Optional.ofNullable(username)
                        .orElseGet(() -> context.getBot().getBotUsername())::equals);
    }

    public static JoinedCondition self() {
        return new JoinedCondition(null);
    }

    /**
     * @param username nullable (same as `JoinedCondition::self` when username is null)
     */
    public static JoinedCondition user(final String username) {
        return new JoinedCondition(username);
    }
}
