package pl.tsmiatacz.botplatform.behavior.condition;

import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

public class PhotoInReplyCondition extends Condition {

    private static final PhotoInReplyCondition INSTANCE = new PhotoInReplyCondition();

    private PhotoInReplyCondition() {
        // intentionally left blank
    }

    @Override
    protected boolean check(final BehaviorContext context) {
        return context.getUpdate().hasMessage()
                && context.getUpdate().getMessage().isReply()
                && context.getUpdate().getMessage().getReplyToMessage().hasPhoto();
    }

    public static PhotoInReplyCondition of() {
        return INSTANCE;
    }
}
