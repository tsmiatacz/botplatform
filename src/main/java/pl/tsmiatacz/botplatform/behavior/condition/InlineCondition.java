package pl.tsmiatacz.botplatform.behavior.condition;

import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

/**
 * Checks if bot received an inline command
 */
public class InlineCondition extends Condition {

    private static final InlineCondition INSTANCE = new InlineCondition();

    private InlineCondition() {
        // intentionally left blank
    }

    @Override
    protected boolean check(final BehaviorContext context) {
        return context.getUpdate().hasInlineQuery();
    }

    public static InlineCondition of() {
        return INSTANCE;
    }
}
