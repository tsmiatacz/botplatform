package pl.tsmiatacz.botplatform.behavior.condition;

import org.apache.commons.lang3.StringUtils;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.MessageEntity;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import java.util.Collections;
import java.util.Objects;
import java.util.Optional;

import static org.telegram.telegrambots.meta.api.objects.EntityType.BOTCOMMAND;

/**
 * Checks if message is a command, eg. "/ping"
 */
public class CommandCondition extends Condition {

    private final String command;

    private CommandCondition(final String command) {
        Objects.requireNonNull(command, "command cannot be null");
        this.command = command;
    }

    @Override
    protected boolean check(final BehaviorContext context) {
        if (!context.getUpdate().hasMessage()) {
            return false;
        }
        final String receivedCommand = getBotCommand(context.getUpdate().getMessage());
        if (context.isUserChat()) {
            return StringUtils.equalsIgnoreCase(receivedCommand, command);
        }
        return StringUtils.equalsIgnoreCase(receivedCommand, command + "@" + context.getBot().getBotUsername());
    }

    protected String getBotCommand(final Message message) {
        return Optional.ofNullable(message.getEntities())
                .orElseGet(Collections::emptyList).stream()
                .filter(entity -> BOTCOMMAND.equalsIgnoreCase(entity.getType()))
                .map(MessageEntity::getText)
                .findFirst()
                .orElse(StringUtils.EMPTY);
    }

    /**
     * @param command not null
     */
    public static CommandCondition of(final String command) {
        return new CommandCondition(command);
    }
}
