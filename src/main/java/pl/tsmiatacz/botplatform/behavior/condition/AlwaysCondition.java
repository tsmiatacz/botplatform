package pl.tsmiatacz.botplatform.behavior.condition;

import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

/**
 * Always true
 */
public class AlwaysCondition extends Condition {

    private static final AlwaysCondition INSTANCE = new AlwaysCondition();

    private AlwaysCondition() {
        // intentionally left blank
    }

    @Override
    protected boolean check(final BehaviorContext context) {
        return true;
    }

    public static AlwaysCondition of() {
        return INSTANCE;
    }
}
