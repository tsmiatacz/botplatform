package pl.tsmiatacz.botplatform.behavior.condition;

import org.apache.commons.lang3.StringUtils;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.MessageEntity;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

import static org.telegram.telegrambots.meta.api.objects.EntityType.MENTION;

/**
 * Checks if bot was addressed with given message (ie. was mentioned unless it's in user chat)
 */
public class MessageCondition extends Condition {

    private final Predicate<String> predicate;

    private MessageCondition(final Predicate<String> predicate) {
        this.predicate = predicate;
    }

    @Override
    protected boolean check(final BehaviorContext context) {
        if (!context.getUpdate().hasMessage()) {
            return false;
        }

        final var message = context.getUpdate().getMessage();
        if (!wasBotAddressed("@" + context.getBot().getBotUsername(), message, context.isUserChat())) {
            return false;
        }

        return predicate.test(message.getText());
    }

    private boolean wasBotAddressed(final String botUsername, final Message message, final boolean isUserChat) {
        if (isUserChat) {
            return true;
        }

        return Optional.ofNullable(message.getEntities())
                .orElseGet(Collections::emptyList).stream()
                .filter(entity -> MENTION.equals(entity.getType()))
                .map(MessageEntity::getText)
                .anyMatch(botUsername::equals);
    }

    /**
     * @param request not null
     */
    public static MessageCondition contains(final String request) {
        Objects.requireNonNull(request, "request cannot be null");
        return new MessageCondition(s -> StringUtils.contains(s, request));
    }

    /**
     * @param request not null
     */
    public static MessageCondition endsWith(final String request) {
        Objects.requireNonNull(request, "request cannot be null");
        return new MessageCondition(s -> StringUtils.endsWithIgnoreCase(s, request));
    }
}
