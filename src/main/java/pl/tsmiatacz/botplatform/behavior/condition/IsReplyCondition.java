package pl.tsmiatacz.botplatform.behavior.condition;

import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

/**
 * Checks if message is a reply.
 */
public class IsReplyCondition extends Condition {

    private static final IsReplyCondition INSTANCE = new IsReplyCondition();

    private IsReplyCondition() {
        // intentionally left blank
    }

    @Override
    protected boolean check(final BehaviorContext context) {
        return context.getUpdate().hasMessage()
                && context.getUpdate().getMessage().isReply();
    }

    public static IsReplyCondition of() {
        return INSTANCE;
    }
}
