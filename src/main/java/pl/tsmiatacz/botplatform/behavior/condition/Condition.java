package pl.tsmiatacz.botplatform.behavior.condition;

import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import java.util.Objects;
import java.util.function.Predicate;

public abstract class Condition implements Predicate<BehaviorContext> {

    /**
     * @param context not null
     */
    protected abstract boolean check(BehaviorContext context);

    @Override
    public final boolean test(BehaviorContext context) {
        Objects.requireNonNull(context, "context cannot be null");
        return check(context);
    }
}
