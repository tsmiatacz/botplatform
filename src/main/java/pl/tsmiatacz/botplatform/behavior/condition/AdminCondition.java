package pl.tsmiatacz.botplatform.behavior.condition;

import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

/**
 * Checks if update comes from admin user
 */
public class AdminCondition extends Condition {

    private static final AdminCondition INSTANCE = new AdminCondition();

    private AdminCondition() {
        // intentionally left blank
    }

    @Override
    protected boolean check(final BehaviorContext context) {
        if (!context.getUpdate().hasMessage()) {
            return false;
        }

        return context.getBot().getBotAdmin()
                .equals(context.getFrom().getUserName());
    }

    public static AdminCondition of() {
        return INSTANCE;
    }

}
