package pl.tsmiatacz.botplatform.behavior.condition;

import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

public class PhotoCondition extends Condition {

    private static final PhotoCondition INSTANCE = new PhotoCondition();

    private PhotoCondition() {
        // intentionally left blank
    }

    @Override
    protected boolean check(final BehaviorContext context) {
        return context.getUpdate().hasMessage()
                && context.getUpdate().getMessage().hasPhoto();
    }

    public static PhotoCondition of() {
        return INSTANCE;
    }
}
