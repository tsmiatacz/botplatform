package pl.tsmiatacz.botplatform.behavior;

public class BehaviorException extends RuntimeException{
    public BehaviorException(final String message) {
        super(message);
    }
}
