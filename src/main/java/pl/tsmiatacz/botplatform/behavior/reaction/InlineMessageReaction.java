package pl.tsmiatacz.botplatform.behavior.reaction;

import org.telegram.telegrambots.meta.api.methods.AnswerInlineQuery;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.objects.inlinequery.inputmessagecontent.InputTextMessageContent;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResult;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResultArticle;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;
import pl.tsmiatacz.botplatform.data.TextData;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * React with message to inline query
 */
public class InlineMessageReaction extends BotReaction<Boolean> {

    private final Function<String, List<TextData>> messageProvider;

    private InlineMessageReaction(final Function<String, List<TextData>> messageProvider) {
        Objects.requireNonNull(messageProvider, "message provider cannot be null");
        this.messageProvider = messageProvider;
    }

    @Override
    protected BotApiMethod<Boolean> createReaction(final BehaviorContext context) {
        final List<TextData> messages = messageProvider.apply(context.getUpdate().getInlineQuery().getQuery());

        final List<InlineQueryResult> texts = messages.stream()
                .distinct()
                .map(text -> InlineQueryResultArticle.builder()
                        .id(text.getId())
                        .title(" ")
                        .description(text.getText())
                        .inputMessageContent(
                                InputTextMessageContent.builder()
                                        .messageText(text.getText())
                                        .build())
                        .build())
                .collect(Collectors.toList());

        return AnswerInlineQuery.builder()
                .inlineQueryId(context.getUpdate().getInlineQuery().getId())
                .results(texts)
                .build();
    }

    /**
     * @param messageProvider not null
     */
    public static InlineMessageReaction of(final Function<String, List<TextData>> messageProvider) {
        return new InlineMessageReaction(messageProvider);
    }
}
