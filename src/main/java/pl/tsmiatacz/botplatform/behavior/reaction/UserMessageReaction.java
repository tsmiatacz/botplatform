package pl.tsmiatacz.botplatform.behavior.reaction;

import org.apache.commons.lang3.StringUtils;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.MessageEntity;
import org.telegram.telegrambots.meta.api.objects.User;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.telegram.telegrambots.meta.api.objects.EntityType.MENTION;
import static org.telegram.telegrambots.meta.api.objects.EntityType.TEXTMENTION;

/**
 * Will address all mentioned users with given message.
 * Bot itself and bot admin are ignored.
 */
public class UserMessageReaction extends Reaction {

    private final Supplier<String> messageSupplier;

    public UserMessageReaction(final Supplier<String> messageSupplier) {
        this.messageSupplier = messageSupplier;
    }

    @Override
    protected void react(final BehaviorContext context) {
        final var usernameMentions = getUsernameMentions(context.getUpdate().getMessage())
                .filter(user -> !StringUtils.equalsIgnoreCase(user, "@" + context.getBot().getBotUsername()))
                .filter(user -> !StringUtils.equalsIgnoreCase(user, "@" + context.getBot().getBotAdmin()))
                .collect(Collectors.toSet());

        final var userMentions = getUserMentions(context.getUpdate().getMessage())
                .collect(Collectors.toSet());

        final var chatId = context.getChatId();
        usernameMentions.stream()
                .map(user -> createMessage(chatId, user , messageSupplier.get(), null))
                .forEach(context::execute);

        userMentions.stream()
                .map(user -> createMessage(chatId, user, messageSupplier.get(), ParseMode.MARKDOWN))
                .forEach(context::execute);
    }

    private SendMessage createMessage(final String chatId, final String user, final String message, final String parseMode) {
        final var sendMessage = new SendMessage(chatId, user + " " + message);

        if (parseMode != null) {
            sendMessage.setParseMode(parseMode);
        }

        return sendMessage;
    }

    private Stream<String> getUsernameMentions(final Message message) {
        return Optional.ofNullable(message.getEntities())
                .orElseGet(Collections::emptyList).stream()
                .filter(entity -> MENTION.equalsIgnoreCase(entity.getType()))
                .map(MessageEntity::getText);
    }

    private Stream<String> getUserMentions(final Message message) {
        return Optional.ofNullable(message.getEntities())
                .orElseGet(Collections::emptyList).stream()
                .filter(entity -> TEXTMENTION.equalsIgnoreCase(entity.getType()))
                .map(MessageEntity::getUser)
                .map(this::userToString);
    }

    protected String userToString(final User user) {
        if (user.getUserName() != null) {
            return "@" + user.getUserName();
        }

        return String.format("[%s](tg://user?id=%s)",
                user.getFirstName(), user.getId());
    }

    /**
     * @param message not null
     */
    public static UserMessageReaction of(final String message) {
        Objects.requireNonNull(message, "message cannot be null");
        return new UserMessageReaction(() -> message);
    }

    /**
     * @param messageSupplier not null
     */
    public static UserMessageReaction of(final Supplier<String> messageSupplier) {
        return new UserMessageReaction(messageSupplier);
    }
}
