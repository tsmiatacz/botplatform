package pl.tsmiatacz.botplatform.behavior.reaction;

import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

/**
 * React by reloading bot
 */
public class ReloadDataReaction extends Reaction {

    private static final ReloadDataReaction INSTANCE = new ReloadDataReaction();

    private ReloadDataReaction() {
        // intentionally left blank
    }

    @Override
    protected void react(final BehaviorContext context) {
        context.getBot().initialize();
    }

    public static ReloadDataReaction of() {
        return INSTANCE;
    }
}
