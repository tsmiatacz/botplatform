package pl.tsmiatacz.botplatform.behavior.reaction;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import java.util.Objects;

/**
 * React with bot info message
 */
public class BotInfoReaction extends BotReaction<Message> {

    private final String infoTemplate;

    private BotInfoReaction(final String infoTemplate) {
        Objects.requireNonNull(infoTemplate, "infoTemplate cannot be null");
        this.infoTemplate = infoTemplate;
    }

    @Override
    protected SendMessage createReaction(final BehaviorContext context) {
        return new SendMessage(context.getChatId(), createInfo(context));
    }

    private String createInfo(final BehaviorContext context) {
        var message = infoTemplate;

        final var user = context.getBot().getUser();

        message = replace(message, "${firstName}", user.getFirstName());
        message = replace(message, "${lastName}", user.getLastName());
        message = replace(message, "${userName}", user.getUserName());
        message = replace(message, "${userId}", String.valueOf(user.getId()));
        message = replace(message, "${chatId}", String.valueOf(context.getChatId()));

        return message;
    }

    private String replace(final String message, final String placeholder, final String value) {
        if (message.contains(placeholder)) {
            return message.replace(placeholder, value);
        }
        return message;
    }

    /**
     * @param infoTemplate not null, can have following placeholders:
     *                     - ${firstName}
     *                     - ${lastName}
     *                     - ${userName}
     *                     - ${userId}
     *                     - ${chatId}
     */
    public static BotInfoReaction of(final String infoTemplate) {
        return new BotInfoReaction(infoTemplate);
    }
}
