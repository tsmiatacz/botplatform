package pl.tsmiatacz.botplatform.behavior.reaction;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.groupadministration.LeaveChat;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

/**
 * This reaction will cause bot to leave:
 * - a chat with given id if provided as an argument
 * - current chat otherwise
 */
public class LeaveReaction extends BotReaction<Boolean> {

    private static final LeaveReaction INSTANCE = new LeaveReaction();

    private LeaveReaction() {
        // intentionally left blank
    }

    @Override
    protected BotApiMethod<Boolean> createReaction(final BehaviorContext context) {
        return LeaveChat.builder()
                .chatId(chatIdToLeave(context))
                .build();
    }

    private String chatIdToLeave(final BehaviorContext context) {
        return context.getArguments().stream()
                .findFirst()
                .orElseGet(context::getChatId);
    }

    public static LeaveReaction of() {
        return INSTANCE;
    }
}
