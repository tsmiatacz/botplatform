package pl.tsmiatacz.botplatform.behavior.reaction;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import java.util.Objects;
import java.util.function.Supplier;

/**
 * React with message
 */
public class MessageReaction extends BotReaction<Message> {

    private final Supplier<String> messageSupplier;
    private final boolean reply;
    private final boolean inGroup;

    private MessageReaction(final Supplier<String> messageSupplier, final boolean reply, final boolean inGroup) {
        this.messageSupplier = messageSupplier;
        this.reply = reply;
        this.inGroup = inGroup;
    }

    @Override
    protected SendMessage createReaction(final BehaviorContext context) {
        return createMessage(messageSupplier.get(), context);
    }

    private SendMessage createMessage(final String text, final BehaviorContext context) {
        final var sendMessage = new SendMessage(context.getChatId(), text);
        if (reply) {
            sendMessage.setReplyToMessageId(context.getReplyTo());
        }
        if (inGroup && !context.getArguments().isEmpty()) {
            sendMessage.setChatId(context.argument(0));
        }
        return sendMessage;
    }

    /**
     * @param message not null
     */
    public static MessageReaction simple(final String message) {
        Objects.requireNonNull(message, "message cannot be null");
        return new MessageReaction(() -> message, false, false);
    }

    /**
     * @param messageSupplier not null
     */
    public static MessageReaction simple(final Supplier<String> messageSupplier) {
        Objects.requireNonNull(messageSupplier, "messageSupplier cannot be null");
        return new MessageReaction(messageSupplier, false, false);
    }

    /**
     * @param message not null
     */
    public static MessageReaction reply(final String message) {
        Objects.requireNonNull(message, "message cannot be null");
        return new MessageReaction(() -> message, true, false);
    }

    /**
     * @param messageSupplier not null
     */
    public static MessageReaction reply(final Supplier<String> messageSupplier) {
        Objects.requireNonNull(messageSupplier, "messageSupplier cannot be null");
        return new MessageReaction(messageSupplier, true, false);
    }

    /**
     * TODO refactor this use case
     * @param messageSupplier not null
     */
    public static MessageReaction inGroup(final Supplier<String> messageSupplier) {
        Objects.requireNonNull(messageSupplier, "messageSupplier cannot be null");
        return new MessageReaction(messageSupplier, false, true);
    }
}
