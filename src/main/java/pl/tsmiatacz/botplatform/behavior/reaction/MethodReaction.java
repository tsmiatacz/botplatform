package pl.tsmiatacz.botplatform.behavior.reaction;

import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import java.util.List;

/**
 * React with a method that uses arguments from context
 */
public abstract class MethodReaction extends Reaction {

    /**
     * @param arguments not null
     */
    protected abstract void execute(final List<String> arguments);

    @Override
    public final void react(BehaviorContext context) {
        execute(context.getArguments());
    }
}
