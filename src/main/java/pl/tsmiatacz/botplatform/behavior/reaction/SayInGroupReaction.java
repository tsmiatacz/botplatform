package pl.tsmiatacz.botplatform.behavior.reaction;

import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;
import pl.tsmiatacz.botplatform.behavior.BehaviorException;

/**
 * React by sending a message a specified group
 * Arguments: "{groupId}" "{message}"
 */
public class SayInGroupReaction extends BotReaction<Message> {

    private static final SayInGroupReaction INSTANCE = new SayInGroupReaction();

    private SayInGroupReaction() {
        // intentionally left blank
    }

    @Override
    protected BotApiMethod<Message> createReaction(final BehaviorContext context) {
        if (context.getArguments().size() < 2) {
            throw new BehaviorException("Invalid number of arguments");
        }

        final var chatId = context.argument(0);
        return new SendMessage(chatId, context.argument(1));
    }

    public static SayInGroupReaction of() {
        return INSTANCE;
    }
}
