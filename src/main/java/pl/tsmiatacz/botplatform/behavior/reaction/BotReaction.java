package pl.tsmiatacz.botplatform.behavior.reaction;

import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import java.io.Serializable;

/**
 * React by executing bot method
 */
public abstract class BotReaction<T extends Serializable> extends Reaction {

    /**
     * @param context not null
     */
    protected abstract PartialBotApiMethod<T> createReaction(BehaviorContext context);

    @Override
    public final void react(BehaviorContext context) {
        context.execute(createReaction(context));
    }

}
