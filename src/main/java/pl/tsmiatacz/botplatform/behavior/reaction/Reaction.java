package pl.tsmiatacz.botplatform.behavior.reaction;

import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import java.util.Objects;
import java.util.function.Consumer;

public abstract class Reaction implements Consumer<BehaviorContext> {

    /**
     * @param context not null
     */
    protected abstract void react(BehaviorContext context);

    @Override
    public final void accept(BehaviorContext context) {
        Objects.requireNonNull(context, "context cannot be null");
        react(context);
    }
}
