package pl.tsmiatacz.botplatform.behavior.reaction;

import com.google.common.collect.Lists;
import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

/**
 * React with one of given reactions at random
 */
public class RandomReaction extends Reaction {

    private final List<? extends Consumer<BehaviorContext>> possibleReactions;

    private RandomReaction(final List<? extends Consumer<BehaviorContext>> possibleReactions) {
        Objects.requireNonNull(possibleReactions, "possibleReactions cannot be null");
        this.possibleReactions = possibleReactions;
    }

    @Override
    protected void react(final BehaviorContext context) {
        randomReaction().accept(context);
    }

    private Consumer<BehaviorContext> randomReaction() {
        final List<Consumer<BehaviorContext>> reactions = Lists.newArrayList(possibleReactions);
        Collections.shuffle(reactions);
        return reactions.get(0);
    }

    /**
     * @param possibleReactions not null, not empty
     */
    public static RandomReaction of(final List<? extends Consumer<BehaviorContext>> possibleReactions) {
        return new RandomReaction(possibleReactions);
    }
}
