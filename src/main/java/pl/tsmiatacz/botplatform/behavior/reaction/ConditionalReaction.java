package pl.tsmiatacz.botplatform.behavior.reaction;

import pl.tsmiatacz.botplatform.behavior.BehaviorContext;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Use different reaction depending on given condition
 */
public class ConditionalReaction extends Reaction {

    private final Predicate<BehaviorContext> condition;
    private final Consumer<BehaviorContext> reaction;
    private final Consumer<BehaviorContext> denial;

    protected ConditionalReaction(final Predicate<BehaviorContext> condition,
                               final Consumer<BehaviorContext> reaction,
                               final Consumer<BehaviorContext> denial) {
        Objects.requireNonNull(condition, "confition cannot be null");
        Objects.requireNonNull(condition, "reaction cannot be null");
        Objects.requireNonNull(condition, "denial cannot be null");

        this.condition = condition;
        this.reaction = reaction;
        this.denial = denial;
    }

    @Override
    protected void react(final BehaviorContext context) {
        if (condition.test(context)) {
            reaction.accept(context);
        } else {
            denial.accept(context);
        }
    }

    /**
     * @param condition not null
     * @param reaction not null
     * @param denial not null
     */
    public static ConditionalReaction of(final Predicate<BehaviorContext> condition,
                                         final Consumer<BehaviorContext> reaction,
                                         final Consumer<BehaviorContext> denial) {
        return new ConditionalReaction(condition, reaction, denial);
    }
}
