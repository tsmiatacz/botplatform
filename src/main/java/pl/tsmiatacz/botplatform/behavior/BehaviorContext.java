package pl.tsmiatacz.botplatform.behavior;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.PartialBotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import pl.tsmiatacz.botplatform.BotException;
import pl.tsmiatacz.botplatform.bots.AbstractBot;

import java.io.Serializable;
import java.util.List;

public interface BehaviorContext {

    Logger LOG = LogManager.getLogger(AbstractBot.class);

    AbstractBot getBot();
    Update getUpdate();
    String getChatId();
    Integer getReplyTo();
    boolean isUserChat();
    List<String> getArguments();
    User getFrom();

    default String argument(final int index) {
        return getArguments().get(index);
    }

    default <T extends Serializable> T execute(final PartialBotApiMethod<T> method) {
        try {
            if (method instanceof BotApiMethod) {
                return getBot().execute((BotApiMethod<T>) method);
            } else if (method instanceof SendPhoto) {
                return (T) getBot().execute((SendPhoto) method); // TODO refactor this
            } else {
                throw new BotException("Attempt of executing illegal method: " + method.toString());
            }
        } catch (TelegramApiException e) {
            LOG.error(e);
            throw new BotException(e);
        }
    }
}
