package pl.tsmiatacz.botplatform.behavior;

import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import pl.tsmiatacz.botplatform.bots.AbstractBot;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Static context to use outside and update.
 * Use with caution as this is prone to NPEs.
 */
public class StaticBehaviorContext implements BehaviorContext {

    private final AbstractBot bot;
    private Update update;
    private String chatId;
    private Integer replyTo;
    private User from;
    private boolean isUserChat = false;
    private List<String> arguments = Collections.emptyList();

    private StaticBehaviorContext(final AbstractBot bot) {
        Objects.requireNonNull(bot, "bot cannot be null");
        this.bot = bot;
    }

    @Override
    public AbstractBot getBot() {
        return bot;
    }

    @Override
    public Update getUpdate() {
        return update;
    }

    @Override
    public String getChatId() {
        return chatId;
    }

    @Override
    public Integer getReplyTo() {
        return replyTo;
    }

    @Override
    public boolean isUserChat() {
        return isUserChat;
    }

    @Override
    public User getFrom() {
        return from;
    }

    @Override
    public List<String> getArguments() {
        return arguments;
    }

    public static Builder builder(final AbstractBot bot) {
        return new Builder(bot);
    }

    public static class Builder {

        private final StaticBehaviorContext instance;

        public Builder(final AbstractBot bot) {
            instance = new StaticBehaviorContext(bot);
        }

        public Builder withUpdate(final Update update) {
            instance.update = update;
            return this;
        }

        public Builder withChatId(final String chatId) {
            instance.chatId = chatId;
            return this;
        }

        public Builder withFrom(final User from) {
            instance.from = from;
            return this;
        }

        public Builder withReplyTo(final Integer replyTo) {
            instance.replyTo = replyTo;
            return this;
        }

        public Builder withIsUserChat(final boolean isUserChat) {
            instance.isUserChat = isUserChat;
            return this;
        }

        public Builder withArguments(final String... arguments) {
            instance.arguments = Stream.of(arguments).collect(Collectors.toList());
            return this;
        }

        public StaticBehaviorContext build() {
            return instance;
        }
    }
}
