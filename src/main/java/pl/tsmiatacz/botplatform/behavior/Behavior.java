package pl.tsmiatacz.botplatform.behavior;

import pl.tsmiatacz.botplatform.behavior.condition.AlwaysCondition;
import pl.tsmiatacz.botplatform.behavior.handler.GroupLogExceptionHandler;

import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Behavior {

    private final Predicate<BehaviorContext> condition;
    private final Consumer<BehaviorContext> reaction;
    private final BiConsumer<Exception, BehaviorContext> exceptionHandler;

    private Behavior(final Predicate<BehaviorContext> condition,
                     final Consumer<BehaviorContext> reaction,
                     final BiConsumer<Exception, BehaviorContext> exceptionHandler) {

        Objects.requireNonNull(condition, "condition cannot be null");
        Objects.requireNonNull(condition, "reaction cannot be null");
        Objects.requireNonNull(condition, "exceptionHandler cannot be null");

        this.condition = condition;
        this.reaction = reaction;
        this.exceptionHandler = exceptionHandler;
    }

    public void evaluate(final BehaviorContext context) {
        try {
            if (condition.test(context)) {
                reaction.accept(context);
            }
        } catch (final Exception e) {
            exceptionHandler.accept(e, context);
        }
    }

    public static Behavior of(final Predicate<BehaviorContext> condition,
                              final Consumer<BehaviorContext> reaction,
                              final BiConsumer<Exception, BehaviorContext> exceptionHandler) {
        return new Behavior(condition, reaction, exceptionHandler);
    }

    public static Behavior of(final Predicate<BehaviorContext> condition, final Consumer<BehaviorContext> reaction) {
        return new Behavior(condition, reaction, GroupLogExceptionHandler.of());
    }

    public static Behavior of(final Consumer<BehaviorContext> action) {
        return new Behavior(AlwaysCondition.of(), action, GroupLogExceptionHandler.of());
    }
}
