package pl.tsmiatacz.botplatform.utils;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.Arrays;
import java.util.List;

/**
 * TODO
 */
public class InlineKeyboardBuilder {

    private final List<List<InlineKeyboardButton>> allButtons = Lists.newArrayList();

    public InlineKeyboardBuilder row(final InlineKeyboardButton... buttons) {
        allButtons.add(ImmutableList.copyOf(buttons));
        return this;
    }

    public InlineKeyboardBuilder buttons(final InlineKeyboardButton... buttons) {
        return buttons(Arrays.asList(buttons));
    }

    public InlineKeyboardBuilder buttons(final List<InlineKeyboardButton> buttons) {
        allButtons.addAll(Lists.partition(ImmutableList.copyOf(buttons), 3));
        return this;
    }

    public InlineKeyboardMarkup build() {
        return InlineKeyboardMarkup.builder()
                .keyboard(allButtons)
                .build();
    }

    public static InlineKeyboardButton button(final String label, final String callback) {
        return InlineKeyboardButton.builder()
                .text(label)
                .callbackData(callback)
                .build();
    }

    public static InlineKeyboardBuilder keyboard() {
        return new InlineKeyboardBuilder();
    }
}
