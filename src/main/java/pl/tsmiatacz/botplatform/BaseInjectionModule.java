package pl.tsmiatacz.botplatform;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;
import com.google.inject.matcher.AbstractMatcher;
import com.google.inject.spi.InjectionListener;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;
import pl.tsmiatacz.botplatform.bots.AbstractBot;
import pl.tsmiatacz.botplatform.bots.BotFactory;
import pl.tsmiatacz.botplatform.bots.impl.DefaultBotFactory;
import pl.tsmiatacz.botplatform.service.ConfigurationService;
import pl.tsmiatacz.botplatform.service.InitializerService;
import pl.tsmiatacz.botplatform.service.impl.DefaultConfigurationService;
import pl.tsmiatacz.botplatform.service.TimeService;
import pl.tsmiatacz.botplatform.service.impl.DefaultInitializerService;
import pl.tsmiatacz.botplatform.service.impl.DefaultTimeService;

public class BaseInjectionModule extends AbstractModule {

    private String profile;

    public BaseInjectionModule(final String profile) {
        this.profile = profile;
    }

    @Override
    protected void configure() {
        super.configure();
        bindListener(new BotMatcher(), new BotInitListener());

        bind(ConfigurationService.class).to(DefaultConfigurationService.class).in(Singleton.class);
        bind(BotFactory.class).to(DefaultBotFactory.class).in(Singleton.class);
        bind(TimeService.class).to(DefaultTimeService.class).in(Singleton.class);
        bind(InitializerService.class).to(DefaultInitializerService.class).in(Singleton.class);
    }

    @Provides
    public String provideProfile() {
        return profile;
    }

    private static class BotMatcher extends AbstractMatcher<TypeLiteral<?>> {
        @Override
        public boolean matches(final TypeLiteral<?> typeLiteral) {
            return AbstractBot.class.isAssignableFrom(typeLiteral.getRawType());
        }
    }

    private static class BotInitListener implements TypeListener {
        @Override
        public <I> void hear(final TypeLiteral<I> type, final TypeEncounter<I> encounter) {
            encounter.register((InjectionListener<I>) injectee -> ((AbstractBot) injectee).initialize());
        }
    }
}
