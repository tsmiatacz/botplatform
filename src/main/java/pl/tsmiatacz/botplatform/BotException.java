package pl.tsmiatacz.botplatform;

public class BotException extends RuntimeException {

    public BotException(final String s) {
        super(s);
    }

    public BotException(final Throwable throwable) {
        super(throwable);
    }
}
