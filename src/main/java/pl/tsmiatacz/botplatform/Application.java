package pl.tsmiatacz.botplatform;

import com.google.common.collect.ImmutableList;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import pl.tsmiatacz.botplatform.service.InitializerService;

import java.util.Arrays;

public class Application {

    private static Injector injector;

    private Application() {
        // intentionally left blank
    }

    public static synchronized void run(final String profile, final AbstractModule... optionalModules) throws TelegramApiException {
        if (injector != null) {
            throw new IllegalStateException("Application already running!");
        }

        final ImmutableList<AbstractModule> modules = ImmutableList.<AbstractModule> builder()
                .add(new BaseInjectionModule(profile))
                .addAll(Arrays.asList(optionalModules))
                .build();
        injector = Guice.createInjector(modules);
        final InitializerService initializer = injector.getInstance(InitializerService.class);
        initializer.initialize();
    }

    public static Injector getInjector() {
        return injector;
    }

}
